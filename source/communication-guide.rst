Communication Guide
*******************

We believe that our :doc:`Code of Conduct <code-of-conduct>` can help create a
harassment-free environment, but is not sufficient to create a welcoming
environment on its own. We can all make mistakes: when we do, we take
responsibility for them and try to improve.

This document lays out our gold standard, best practices for some common
situations and mechanisms to help resolve issues that can have a
negative effect on our community.

Goal
====

We want a productive, welcoming and agile community that can welcome new
ideas in a complex technical field which is able to reflect on and improve how
we work.

Communication & Handling Differences in Opinions
================================================

Examples of behavior that contributes to creating a positive environment
include:

* Use welcoming and inclusive language
* Keep discussions technical and actionable
* Be respectful of differing viewpoints and experiences
* Be aware of your own and counterpart’s communication style and culture
* Gracefully accept constructive criticism
* Focus on what is best for the community
* Show empathy towards other community members
* Resolve differences in opinion effectively

Getting Help
============

When developing code collaboratively, technical discussion and disagreements
are unavoidable. Our contributors come from different countries and cultures,
are driven by different goals and take pride in their work and in their point
of view. This invariably can lead to lengthy and unproductive debate,
followed by indecision, sometimes this can impact working relationships
or lead to other issues that can have a negative effect on our community.

To minimize such issue, we provide a 3-stage process

* Self-help as outlined in this document
* Ability to ask for an independent opinion or help in private
* Mediation between parties which disagree. In this case a neutral community
  member assists the disputing parties resolve the issues or will work with the
  parties such that they can improve future interactions.

If you need and independent opinion or help, feel free to contact
mediation@xenproject.org. The team behind mediation@ is made up of the
same community members as those listed in the Conduct Team: see
:doc:`Code of Conduct <code-of-conduct>`. In addition, team members are obligated
to maintain confidentiality with regard discussions that take place. If you
have concerns about any of the members of the mediation@ alias, you are
welcome to contact precisely the team member(s) of your choice. In this case,
please make certain that you highlight the nature of a request by making sure
that either help or mediation is mentioned in the e-mail subject or body.

Specific Topics and Best Practice
=================================

* :doc:`Code Review Guide <code-review-guide>`:
  Essential reading for code reviewers and contributors
* :doc:`Communication Best Practice <communication-practice>`:
  This guide covers communication guidelines for code reviewers and authors.
  It should help you create self-awareness, anticipate, avoid  and help resolve
  communication issues.
* :doc:`Resolving Disagreement <resolving-disagreement>`:
  This guide lays out common situations that can lead to dead-lock and shows
  common patterns on how to avoid and resolve issues.
