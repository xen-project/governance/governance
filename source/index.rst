.. XenProject Governance documentation master file, created by
   sphinx-quickstart on Tue Sep  8 12:10:02 2020.

Welcome to XenProject's Governance Documents
============================================

The Regulations section contains documents
that regulate the operation of the XenProject,
including minimum acceptable behavior
and possible responses to unacceptable behavior.
The Guides section contains documents describing ideal behavior
that we strive for as a community.

.. toctree::
   :maxdepth: 2
   :caption: Regulations:
	     
   core-governance
   code-of-conduct

.. toctree::
   :maxdepth: 2
   :caption: Guides:

   communication-guide
   code-review-guide
   communication-practice
   resolving-disagreement

Other
-----

.. Comment out the index for now, but leave it here so we can think
   about adding it back in later
   * :ref:`genindex`

* :ref:`search`
