Xen Project Code of Conduct
***************************
Last updated: 16th November 2023

Our Pledge
==========
We as members, contributors, and leaders pledge to make participation in our community 
a harassment-free experience for everyone, regardless of age, body size, visible or 
invisible disability, ethnicity, sex characteristics, gender identity and expression, 
level of experience, education, socio-economic status, nationality, personal appearance, 
race, caste, color, religion, or sexual identity and orientation.

We pledge to act and interact in ways that contribute to an open, welcoming, diverse, 
inclusive, and healthy community.

Our Standards
=============

We believe that a Code of Conduct can help create a harassment-free environment,
but is not sufficient to create a welcoming environment on its own: guidance on
creating a welcoming environment, how to communicate in an effective and
friendly way, etc. can be found :doc:`here <communication-guide>`.

Examples of behavior that contributes to a positive environment include:

* Using welcoming and inclusive language
* Demonstrating empathy and kindness towards other people
* Being respectful of differing opinions, viewpoints, and experiences
* Keep the language clean and professional (no swearing)
* Attempting collaboration before conflict 
* Giving and gracefully accepting constructive feedback
* Accepting responsibility and apologizing to those affected by our mistakes, and learning from the experience
* Focusing on what is best not just for us as individuals, but for the overall community

Examples of unacceptable behaviors include:

* Any discriminatory language or behaviour concerning areas described in our pledge, such as race, gender, and ethnicity
* The use of sexualized language or imagery and unwelcome sexual attention or
  advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment both online and in person
* Publishing others' private information, such as a physical or electronic
  address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a
  professional setting
* Advocating for, or encouraging, any of the above behaviour

Our Responsibilities
====================

Project leadership team members are responsible for clarifying the standards of
acceptable behavior and are expected to take appropriate and fair corrective
action in response to any instances of unacceptable behavior.

Project leadership team members have the right and responsibility to remove,
edit, or reject comments, commits, code, wiki edits, issues, and other
contributions that are not aligned to this Code of Conduct, or to ban
temporarily or permanently any contributor for other behaviors that they deem
inappropriate, threatening, offensive, or harmful.

Scope
=====

This Code of Conduct applies within all project spaces of all sub-projects,
and it also applies when an individual is representing the project or its
community in public spaces. Examples of representing a project or community
include using an official project e-mail address, posting via an official social
media account, or acting as an appointed representative at an online or offline
event. Representation of a project may be further defined and clarified by the
project leadership.

What to do if you witness or are subject to unacceptable behavior
=================================================================

Instances of abusive, harassing, or otherwise unacceptable behavior may be
reported to Conduct Team members at conduct@xenproject.org. All
complaints will be reviewed and investigated and will result in a response that
is deemed necessary and appropriate to the circumstances. Conduct Team members
are obligated to maintain confidentiality with regard to the reporter of an
incident. Further details of specific enforcement policies may be posted
separately.

If you have concerns about any of the members of the conduct@ alias,
you are welcome to contact precisely the Conduct Team member(s) of
your choice.

Project leadership team members who do not follow or enforce the Code of Conduct
in good faith may face temporary or permanent repercussions as determined by
other members of the project's leadership.

Conduct Team members
====================

Conduct Team members are project leadership team members from any
sub-project. The current list of Conduct Team members is:

* Roger Pau Monne <roger dot pau at cloud dot com>
* Stefano Stabellini <sstabellini at kernel dot org>

Conduct Team members are changed by proposing a change to this document,
posted on all sub-project lists, followed by a formal global vote as outlined
`here <Project Governance_>`_.

Attribution
===========

This Code of Conduct is adapted from the Contributor Covenant, version 2.1
Available at https://www.contributor-covenant.org/version/2/1/code_of_conduct.html.

For answers to common questions about this code of conduct, see
https://www.contributor-covenant.org/faq

.. _Communication Guide: communication-guide.html
.. _Project Governance: https://xenproject.org/developers/governance/#project-decisions
.. _Contributor Covenant: https://www.contributor-covenant.org
